@extends('layouts.app')

@section('content')
    @include('components.books', [
        'title' => 'Menin kitaplarym',
        'books' => $books
    ])
@stop
