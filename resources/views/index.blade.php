@extends('layouts.app')

@section('content')
    <!-- BOOK CATEGORIES -->
    @include('components.categories')

    @include('components.books', [
        'title' => 'Kitaplar',
        'books' => $books
    ])
@stop
