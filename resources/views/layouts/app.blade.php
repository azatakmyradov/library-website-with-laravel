<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/css/vendor.css">
    <link rel="stylesheet" href="/css/app.css">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body>

    <div id="app">
        <!-- HEADER -->
        @include('components.header')

        @yield('content')

        <!-- FOOTER -->
        @include('components.footer')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    @include('components.alerts')
</body>
</html>
