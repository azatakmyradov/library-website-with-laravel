@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ route('add-category') }}" method="POST" role="form">

            {{ csrf_field() }}

            <legend>Bölüm goş</legend>

            <div class="form-group">
                <label for="name">Ady</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Bölümiň ady">
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>

            <button type="submit" class="btn btn-primary">Goş</button>
        </form>

        <br>

        <div class="list-group">
            @foreach ($categories as $category)
                <a href="{{ route('delete-category', ['id' => $category->id]) }}" class="list-group-item" @click.prevent="confirmFirst($event.target.href)">{{ $category->name }}</a>
            @endforeach
        </div>
    </div>
@stop
