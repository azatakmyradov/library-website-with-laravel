@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('add-book') }}" method="POST" role="form" enctype="multipart/form-data">

            {{ csrf_field() }}

            <legend>Kitap goş</legend>

            <div class="form-group">
                <label for="name">Kitabyň ady</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Kitabyň ady">
            </div>

            <div class="form-group">
                <label for="description">Kitap barada</label>
                <input type="text" class="form-control" id="description" name="description" placeholder="Kitap barada">
            </div>

            <div class="form-group">
                <label for="author">Kitabyň ýazyjysy</label>
                <input type="text" class="form-control" id="author" name="author" placeholder="Kitap barada">
            </div>

            <select class="form-control" name="category_id">
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>

            <div class="form-group">
                <label for="image">Surat saýla</label>
                <input type="file" id="image" name="image">
            </div>

            <div class="form-group">
                <label for="book">Kitap saýla</label>
                <input type="file" id="book" name="book">
            </div>

            <div class="form-group">
                <label for="price">Kitabyň bahasy</label>
                <input type="number" class="form-control" id="price" name="price" placeholder="Kitabyň bahasy">
            </div>

            <button type="submit" class="btn btn-primary">Goý</button>
        </form>
    </div>
@stop
