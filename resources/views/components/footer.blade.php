<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Maglumat</h3>

                    <ul>
                        <li><a href="#">Biz hakda</a></li>
                        <li><a href="#">Biziň işlerimiz</a></li>
                        <li><a href="#">Kanunlar we hukuklar</a></li>
                        <li><a href="#">Habarlaş</a></li>
                    </ul>
                </div><!-- end col-lg-6 -->
                <div class="col-lg-6">
                    <h3>Profilim</h3>

                    <ul>
                        <li><a href="#">Meniň profilim</a></li>
                        <li><a href="#">Islenen kitaplar</a></li>
                        <li><a href="#">Jyrjak</a></li>
                    </ul>
                </div><!-- end col-lg-6 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-top -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <span>Ähli hukuklar goragly © 2017</span>
                </div><!-- end col-lg-6 -->
                <div class="col-lg-6">
                    <div class="social">
                        <ul>
                            <li><a href="#"><i class="la la-vk"></i></a></li>
                            <li><a href="#"><i class="la la-facebook"></i></a></li>
                            <li><a href="#"><i class="la la-twitter"></i></a></li>
                            <li><a href="#"><i class="la la-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div><!-- end col-lg-6 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-bottom -->
</footer><!-- end footer -->
