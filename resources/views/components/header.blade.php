<header class="main-header">
    <div class="top-line">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <nav class="main-nav">
                        <ul>
                            <li><a href="/">Baş sahypa</a></li>
                        </ul>
                    </nav><!-- end main-nav -->
                </div><!-- end col-lg-4 -->
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <nav class="another-nav">
                        <ul>
                            @if (auth()->user())
                                <li>
                                    <a href="#" class="open-dropdown">{{ auth()->user()->name }} - {{ auth()->user()->fortune }} TMT <i class="la la-angle-down"></i></a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('books') }}">Kitaplarym</a></li>
                                        @if (auth()->user()->is_admin)
                                            <li><a href="{{ route('add-book') }}">Kitap goş</a></li>
                                        @endif
                                        <li><a href="/logout">Jyrjak</a></li>
                                    </ul>
                                </li>
                            @else
                                <li><a href="/register">Agza bol</a></li>
                                <li><a href="/login">Içeri gir</a></li>
                            @endif
                        </ul>
                    </nav>
                </div><!-- end col-lg-8 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end top-line -->
    <div class="bottom-line">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="logo">
                        <a href="#"><img src="/images/logo.png" alt="Logo"></a>
                    </div><!-- end logo -->
                </div><!-- end col-lg-6 -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <form action="{{ route('search') }}" method="GET">
                        <input type="text" name="q" class="search">
                        <button type="submit" class="btn">Gözleg <i class="la la-search"></i></button>
                    </form>
                </div><!-- end col-lg-6 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end bottom-line -->
</header><!-- end main-header -->
