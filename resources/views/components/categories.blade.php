<div class="categories">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="fixed-nav">
                    <div class="toggle">
                        <i class="la la-bars"></i>
                    </div>

                    <div class="logo">
                        {{-- <a href="#"><img src="/images/logo.png" alt="Logo"></a> --}}
                        Kitap
                    </div><!-- end logo -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="owl-carousel">
                    @foreach ($categories as $category)
                        <a href="#" class="item">{{ $category->name }}</a>
                    @endforeach
                    @if (auth()->user())
                        @if (auth()->user()->is_admin)
                            <a href="{{ route('add-category') }}" class="item">+</a>
                        @endif
                    @endif
                </div><!-- end owl-carousel -->
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end categories -->
