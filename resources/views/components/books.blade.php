<!-- BOX -->
<div class="box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                @if (count($books) !== 0)
                    <div class="box-heading">
                        {{ $title }}
                    </div>
                @endif

                <div class="box-content">
                    <div class="box-product">
                        <div class="row">
                            @if (count($books) == 0)
                                <div class="col-lg-12">
                                    <div class="text-center">
                                        <div class="no-books">
                                            <i class="la la-exclamation-triangle"></i>
                                            <p>
                                                Kitap yogow sende mal
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @foreach ($books as $book)
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="card">
                                        <div class="card-top">
                                            <a href="#"><img src="{{ $book->image }}" alt="Book cover"></a>
                                        </div>
                                        <div class="card-content">
                                            <h5 class="name"><a href="#">{{ $book->name }}</a></h5>

                                            <p class="description">{{ $book->description }}</p>
                                        </div>
                                        <div class="card-bottom">
                                            <div class="price">
                                                {{ $book->price }} TMT
                                            </div>
                                            <div class="shop">
                                                @if ($book->is_purchased())
                                                    <a href="{{ route('download-book', ['id' => $book->id]) }}"><i class="la la-download"></i></a>
                                                @else
                                                    <a href="{{ route('purchase-book', ['id' => $book->id]) }}"><i class="la la-shopping-cart"></i></a>
                                                @endif
                                                @if (auth()->user())
                                                    @if (auth()->user()->is_admin)
                                                        <a href="{{ route('delete-book', ['id' => $book->id]) }}" @click.prevent="confirmFirst($event.target.href)"><i class="la la-trash"></i></a>
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div> <!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end box -->
