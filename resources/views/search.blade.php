@extends('layouts.app')

@section('content')
    @include('components.books', [
        'title' => 'Gozleg: ' . request()->q,
        'books' => $books
    ])
@stop
