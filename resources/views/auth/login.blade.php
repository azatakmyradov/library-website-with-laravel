@extends('layouts.app')

@section('content')
    <div class="container">

    </div>

    <div class="login box">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="sign-up">
                        <form action="/register" method="POST" role="form">

                            {{ csrf_field() }}

                            <h2 class="text-center">
                                Agza bol <i class="la la-user"></i>
                            </h2>

                            <div class="form-group">
                                <label for="name">Ady:</label>
                                <input type="text" id="name" name="name" placeholder="Ady-ňyzy giriziň">
                            </div>

                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" id="email" name="email" placeholder="Email-iňizi giriziň">
                            </div>

                            <div class="form-group">
                                <label for="password">Açarsöz:</label>
                                <input type="password" id="password" name="password" placeholder="Açarsöz-üňizi giriziň">
                            </div>

                            <button type="submit" class="btn btn-primary">Agza bol</button>
                        </form>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="sign-in">
                        <form action="/login" method="POST" role="form">

                            {{ csrf_field() }}

                            <h2 class="text-center">
                                Icheri gir <i class="la la-key"></i>
                            </h2>

                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Email-iňizi giriziň">
                            </div>

                            <div class="form-group">
                                <label for="password">Açarsöz:</label>
                                <input type="password" id="password" name="password" placeholder="Açarsöz-üňizi giriziň">
                            </div>

                            <button type="submit" class="btn btn-primary">Içeri gir <i class="la la-sign-in"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
