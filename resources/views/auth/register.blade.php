@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="/register" method="POST" role="form">

            {{ csrf_field() }}

            <legend>Agza bol</legend>

            <div class="form-group">
                <label for="name">Ady</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Ady-ňyzy giriziň">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email-iňizi giriziň">
            </div>

            <div class="form-group">
                <label for="password">Açarsöz</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Açarsöz-üňizi giriziň">
            </div>

            <button type="submit" class="btn btn-primary">Agza bol</button>
        </form>
    </div>
@stop
