require('./bootstrap');

window.Vue = require('vue');

const app = new Vue({
    el: '#app',
    methods: {
        confirmFirst(url) {
            var sure = confirm('Çyndanam dowam etmekçimi?');

            if (sure) {
                location = url;
            }
        }
    }
});

$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        autoWidth:true,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:5,
            }
        }
    });
});

// Dropdown
$(document).ready(function()  {
    $('header .open-dropdown').click(function(){
        $(this).next().toggle().addClass('toggle');
    });
});
