<?php

Route::get('/books/create', 'BooksController@showCreateForm')->name('add-book');
Route::post('/books/create', 'BooksController@create');
Route::get('/books/{book}/delete', 'BooksController@delete')->name('delete-book');

Route::get('/category/create', 'CategoriesController@showCreateForm')->name('add-category');
Route::post('/category/create', 'CategoriesController@create');
Route::get('/category/{category}/delete', 'CategoriesController@delete')->name('delete-category');
