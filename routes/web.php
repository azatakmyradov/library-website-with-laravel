<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/search', 'BooksController@search')->name('search');

Route::get('/login', 'LoginController@showForm')->name('login');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::get('/register', 'RegisterController@showForm');
Route::post('/register', 'RegisterController@create');

Route::get('/books', 'BooksController@index')->name('books')->middleware('auth');
Route::get('/books/{book}/purchase', 'BooksController@purchase')->name('purchase-book')->middleware('auth');
Route::get('/books/{book}/download', 'BooksController@download')->name('download-book')->middleware('auth');

Route::middleware('admin')->prefix('admin')->group(function () {
    require('admin.php');
});
