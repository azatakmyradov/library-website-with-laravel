<?php

namespace App\Http\Controllers;

use Storage;
use App\Book;
use App\Category;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index()
    {
        $books = [];
        foreach (auth()->user()->purchases()->with('book')->get() as $book) {
            $books[] = $book->book;
        }

        return view('user.books', [
            'books' => collect($books)
        ]);
    }

    public function showCreateForm()
    {
        return view('admin.books.create', [
            'categories' => \App\Category::all()
        ]);
    }

    public function create()
    {
        $data = request()->validate([
            'name' => 'required|min:4|max:100',
            'description' => 'required|min:4|max:255',
            'author' => 'required|min:2|max:50',
            'category_id' => 'required|in:' . Category::all()->implode('id', ','),
            'image' => 'required',
            'book' => 'required',
            'price' => 'required|max:10'
        ]);

        $data['image'] = str_replace('public', '/storage', request()->file('image')->store('public/images'));
        $data['book'] = request()->file('book')->store('books');

        Book::create($data);

        return back()->withInfo('Kitap goşuldy');
    }

    public function delete(Book $book)
    {
        $book->delete();

        return back()->withInfo('Kitap pozuldy');
    }

    public function purchase(Book $book)
    {
        if ( ! auth()->user()->affords($book->price)) {
            return back()->withInfo('Pulun yetenok');
        }

        $book->buy();

        return redirect('/books');
    }

    public function download(Book $book)
    {
        if ( ! $book->is_purchased()) {
            return back()->withInfo('Birinji satyn almaly');
        }

        return Storage::download($book->book);
    }

    public function search()
    {
        $books = Book::where('name', 'LIKE', '%' . strtolower(request()->q) . '%')
            ->where('author', 'LIKE', '%' . strtolower(request()->q) . '%')
            ->where('description', 'LIKE', '%' . strtolower(request()->q) . '%')
            ->get();

        return view('search', [
            'books' => $books
        ]);
    }
}
