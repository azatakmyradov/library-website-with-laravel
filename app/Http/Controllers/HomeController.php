<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index', [
            'books' => \App\Book::all(),
            'categories' => \App\Category::all()
        ]);
    }
}
