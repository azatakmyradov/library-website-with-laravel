<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function showCreateForm()
    {
        return view('admin.category.create', [
            'categories' => Category::all()
        ]);
    }

    public function create()
    {
        $data = request()->validate([
            'name' => 'required|min:2|max:20'
        ]);

        Category::create($data);

        return back();
    }

    public function delete(Category $category)
    {
        $category->delete();

        return back();
    }
}
