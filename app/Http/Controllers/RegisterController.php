<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function showForm()
    {
        return view('auth.register');
    }

    public function create()
    {
        $data = request()->validate([
            'name' => 'required|min:2|max:20',
            'email' => 'unique:users|required|email|min:4|max:50',
            'password' => 'required|min:5|max:40'
        ]);

        User::create($data);

        Auth::attempt(['email' => request()->email, 'password' => request()->password]);

        return redirect('/');
    }
}
