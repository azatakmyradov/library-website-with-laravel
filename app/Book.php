<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['name', 'description', 'author', 'category_id', 'image', 'book', 'price'];

    public function buy()
    {
        $purchase = new Purchase;
        $purchase->user_id = auth()->user()->id;
        $purchase->book_id = $this->id;
        $purchase->save();

        auth()->user()->charge($this->price);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function is_purchased()
    {
        if ( ! auth()->user()) return false;

        $purchase = auth()->user()->purchases()->where('book_id', $this->id)->first();

        if ( ! $purchase) {
            return false;
        }

        return true;
    }
}
